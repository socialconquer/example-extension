import net.xeoh.plugins.base.annotations.PluginImplementation;
import net.xeoh.plugins.base.annotations.events.Init;
import yourfunworldstudios.socialconquer.Main;
import yourfunworldstudios.socialconquer.DevMechanics.Expansion;

@PluginImplementation
public class Example implements Expansion {

	@Init
	public void onEnable() {
		Main.log.info("Enabled SocialConquer Example extension"); // Uses SocialConquer to print to Console... :>
	}

}
