This is an example of a SocialConquer Extension with no code.


The SocialConquer API consists of no-working functionality code, however you can use this API to create a completely working extension to the SocialConquer code without us compromising our game code.
Once you have a working extension please contact Kayaba and we can deploy and test it on a developer shard.

NOTES:

1) You CANNOT run this standalone, as again it has NO working code... Every single function & method are blanked out to contain no code. Therefore, you cannot use it to test the extension on your own.
2) We CANNOT provide the Spigot build for legal reasons... :> !Use Spigot 1.7.5 for now, as this is what SocialConquer is built upon.
3) All extension submissions MUST be accompanied with corresponding code.. We will NOT push any of your binaries to our live servers. We will instead self compile them and then push them after reviewing your code.

All Bukkit functionality will still obviously work.. Most functions in the SocialConquer API are self explainitory, if not ask a Head Developer. 